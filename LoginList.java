/*
 * LoginList
 * This class represents a data structure containing Peers's name, id 
 */

class LoginList {
	String peername;
	int peerid;
	
	public LoginList(String peername, int peerid) {
		this.peername = peername;
		this.peerid = peerid;
	}
}
