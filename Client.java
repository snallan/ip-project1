/*
 * Client
 * Server IP, Client's Accept Port and ID needs to be changed
 */

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class Client {	
	
		public static void main(String[] args) throws IOException, InterruptedException {
		
		
		String serverHostname = new String ("172.16.0.188");
		int serverport=7734;
		
		Map<String,Integer> hostToport = new HashMap<String,Integer>();
		
		String request = null;
		String response = null;
		String input = null;
		String rfctitle = null;		
		String rfcno = null;
		String version = null;
		String host = null;
		String options = null;
		
		Socket echoSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;
		BufferedReader stdIn = null;
		
		int option = 0;
		int portno = 7744;
        int exitflag = 0;
        if (args.length > 0)
			serverHostname = args[0];
		System.out.println ("Attemping to connect to host " +
				serverHostname + " on port 7734.");
		
		try {
			echoSocket = new Socket(serverHostname, serverport);
			out = new PrintWriter(echoSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(
					echoSocket.getInputStream()));
		
		
		} catch (UnknownHostException e) {
			System.err.println("Unknown host: " + serverHostname);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("I/O exception in "
					+ "the connection to: " + serverHostname);
			System.exit(1);
		}

		stdIn = new BufferedReader(new InputStreamReader(System.in));	

		/* Login */
		// 1. Login information from server
		response = in.readLine();
		System.out.print ("Server: "+response);
		if(response.equals("LOGIN")) {
			out.println("LOGINACK\r\n");
		}
		else if(response.equals("REGISTER")) {
			// get peerid from comandline
			request = "1234\r\n"; //sample peer id
			out.println(request);
		}

		// 2. Login response from server
		response = in.readLine();
		response = in.readLine();
		System.out.println("Server1: "+response);
		
		/* Create thread that handles multiple connect request from other peers */
		Thread acceptThread = new MultipleAccept();
		acceptThread.start();
		
		if(response.equals("LOGIN:ACK")) {
			// Get request and input from user
			System.out.println("Inside login ack");
			host = java.net.InetAddress.getLocalHost().getHostName();
			options = "1.ADD\r\n2.LOOKUP\r\n3.LIST ALL\r\n4.DOWNLOAD\r\n5.EXIT\r\nType the number corresponding to the request\r\n";
			
			while(exitflag == 0) {
				System.out.println(options);								
                option = Integer.parseInt(stdIn.readLine());   
                
				switch(option) {
				case 1:
					System.out.println("Inputs for ADD request");
					System.out.println("Enter the RFC number");
					rfcno = stdIn.readLine();					
					System.out.println("Enter the RFC Title");
					rfctitle = stdIn.readLine();
					System.out.println("Enter the version");
					version = stdIn.readLine();
					// Construct the request message for Look up & send to the client.
					request = "ADD RFC " + rfcno + " " + version + "\r\nHost: "+ host + "\r\nPort: "+ portno + "\r\nTitle: "+ rfctitle + "\r\nEND\r\n"; 
					out.println(request);
					response = null;
					// Get the response from the server
					 while(!(input = in.readLine()).equals("END")) {
							System.out.println("Waiting for more input");
							response += input;
					 }
					 System.out.println("Server: "+response);
					break;
				
				case 2:					
					System.out.println("Lookup");
					System.out.println("Enter the RFC number");
					rfcno = stdIn.readLine();					
					System.out.println("Enter the RFC Title");
					rfctitle = stdIn.readLine();
					System.out.println("Enter the version");
					version = stdIn.readLine();					
					request = "LOOKUP RFC " + rfcno + " " +version + "\r\nHost: "+ host + "\r\nPort: "+ portno + "\r\nTitle: "+ rfctitle + "\r\nEND\r\n"; 
					out.println(request);
					// Get the response from the server
					response = null;
					 while(!(input = in.readLine()).equals("END")) {
							System.out.println("Waiting for more input");
							response += input;
					 }
					 System.out.println("Server: "+response);
					break;
					
				case 3:
					System.out.println("List All");
					String token[];
					hostToport.clear();
					System.out.println("Enter the version");
					version = stdIn.readLine();
					request = "LIST ALL " + version + "\r\nHost: "+ host + "\r\nPort: "+ portno + "\r\nEND\r\n"; 
					out.println(request);
					response = null;
					in.readLine();
					// Get the response from the server					
					 while(!(input = in.readLine()).contains("Empty") && !input.equals("END")) {
							System.out.println("Waiting for more input");
							System.out.println(input+"\n");
							if(response == null)
								response = input;
							else
							{
								response += "\r\n"+input;
								token = input.split(" ");
								hostToport.put(token[2], Integer.parseInt(token[3]));
							}
					 }
					 System.out.println("Server: "+response);
					break;
				case 4:
					String hname,osType;
					System.out.println("Download");
					System.out.println("Enter the RFC number");
					rfcno = stdIn.readLine();					
					System.out.println("Enter the version");
					version = stdIn.readLine();
					System.out.println("Enter the remote host name");
					hname = stdIn.readLine();	
					osType=System.getProperty("os.name");
					int port;
					Thread downloadThread=null;
					if(hostToport.get(hname)!=null)
					{
					port=hostToport.get(hname);
					request = "DOWNLOAD RFC " + rfcno + " " +version + "\r\nHost: "+ hname + "\r\nOS: "+ osType + "\r\nEND\r\n"; 
					downloadThread = new DownloadClient(request,hname,port,rfcno);
					//downloadThread.start();
					int ret;
					ret = ((DownloadClient) downloadThread).threadWrapper(downloadThread);
					
					/*try {
						Thread.sleep(5000);
					}
					catch(Exception e) {
						System.out.println(e.getMessage());
					}*/
					
					if(ret == 1)
					   break;		
					System.out.println(rfcno+" downloaded. DO you want to add it to server(yes/no)?");
					String userresponse = stdIn.readLine();
					if(userresponse.equals("yes"))
					{
						System.out.println("Enter the RFC Title");
						rfctitle = stdIn.readLine();
						
						request = "ADD RFC " + rfcno + " " + version + "\r\nHost: "+ host + "\r\nPort: "+ portno + "\r\nTitle: "+ rfctitle + "\r\nEND\r\n"; 
						out.println(request);
						response = null;
						// Get the response from the server
						 while(!(input = in.readLine()).equals("END")) {
								System.out.println("Waiting for more input");
								response += input;
						 }
						 System.out.println("Server: "+response);
					}
					}
					else
						System.out.println("400 Bad Request \n"+"DOWNLOAD RFC " + rfcno + " " +version + "\r\nHost: "+ hname + "\r\nOS: "+ osType + "\r\nEND\r\n");	
					//downloadThread.join();
					break;
				case 5: 	
					System.out.println("Exiting");
					exitflag = 1;
					break;
				default:
					System.out.println("Invalid option");					
				}              
			}
		}
		
		/* Close Connection */		
		request = "FIN\r\n";
		out.println(request);
		response = in.readLine();
		response = in.readLine();
		System.out.println("Server: "+response);
		if (response.equals("FINACK")) {
			System.out.println("Server sent FINISHED msg");
			request = "ACK\r\n";
			out.println(response);			

			try {
				System.out.println("Sleeping for 2 seconds before closing");
				Thread.sleep(2000);
				out.close();
				in.close();
				stdIn.close();
				echoSocket.close();
			} catch (InterruptedException e) {			
				e.printStackTrace();
			}
		}
	}
}
