import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/*
 * MultipleAccept
 * This class is maintained by client to handle connect request from multiple peers
 */

public class MultipleAccept extends Thread{

	public static int check=1;
	Socket peerSocket;
	
	public MultipleAccept(Socket peerSocket) {
		this.peerSocket=peerSocket;
	}

	public MultipleAccept() {
		// TODO Auto-generated constructor stub
	}

	public void run()
	{
		if(check==1) /* blocks on accept */
		{
			check++;
			
			ServerSocket acceptSocket=null;
			try {
				acceptSocket = new ServerSocket(7744);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			while(true)
			{
				try {
					peerSocket = acceptSocket.accept();
					Thread thr = new MultipleAccept(peerSocket);
					thr.start();
				}
				catch (IOException e)
				{
					System.err.println("Connection failed");
					System.exit(1);
				}
			}
		}
		else /* transfers the RFC to the requesting peer */
		{
			PrintWriter out = null;
			BufferedReader cin = null;
			String request=null;
			String requestRFC=null;
			String token[];
			String statuscode = null;
			try 
			{
				out = new PrintWriter(peerSocket.getOutputStream(),  true);
				cin = new BufferedReader( new InputStreamReader( peerSocket.getInputStream()));
				request=cin.readLine();
				token = request.split(" ");
				requestRFC = token[2];
				//out.println(requestRFC);
				StringBuffer content = new StringBuffer();
				DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Date dt = new Date();
				File file = new File(requestRFC+".txt");
				
				String readline="", version, response;				
				
				// If file does not exist, set the status code
				if(!file.exists()) {
					response = token[3] + " 404 Not Found\r\n" + "\r\n" + request + "\r\n";
					out.println(response);					
					peerSocket.close();
					return;
				}
								
				BufferedReader br = new BufferedReader(new FileReader(requestRFC+".txt"));
				readline = br.readLine();
				
				// If the version doesn't match, set the statuscode. current version is "P2P-CI/1.0"
				if(readline.startsWith("Version:")) {
					version = readline.split(":")[1];
					if(!version.equals(token[3])) {
						statuscode = " 505 P2P-CI Version Not Supported\r\n";
						response = token[3] + statuscode + "\r\n" + request + "\r\n";
						out.println(response);
						br.close();
						peerSocket.close();
						return;
					}
				}			
				
				// If the statuscode is not null, write the statuscode to the socket and terminate							
				response = token[3] + " 200 OK\r\nDate: " + df.format(dt) + "\r\nOS: " + System.getProperty("os.name") + "\r\nLast-Modified: " + file.lastModified();
				response += "\r\nContent-Length: "+ file.length() + "\r\nContent-Type: text/text\r\nData:\r\n";       
				content.append(response);
				content.append(readline+"\n");
				
				while((readline=br.readLine())!=null)
				{
					content.append(readline+"\n");
					//out.println(readline);
				}
				br.close();				
				content.append("END\r\n");
				out.println(content);
				peerSocket.close();
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
