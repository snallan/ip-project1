/*
 * RFCList
 * This class represents a data structure containing FRC's number, title, version and owner of the RFC 
 */

class RFCList {
	String rfctitle, peername, version;
	int rfcno;
	
	public RFCList(String peername, int rfcno, String rfctitle, String version) {
		this.peername = peername;
		this.rfcno = rfcno;
		this.rfctitle = rfctitle;
		this.version = version;
	}
}