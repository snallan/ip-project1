import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/*
 * DownloadClient
 * This class connects to remote peer to download RFC
 */

public class DownloadClient extends Thread{

	String request,hostname,rfcno;
	static int ret;
	int port;
	public DownloadClient(String request, String hostname, int port, String rfcno) {
		this.request=request;
		this.hostname=hostname;
		this.port=port;
		this.rfcno=rfcno;
	}
	
	public int threadWrapper(Thread thr) {
	   thr.start();
	   try {
		thr.join();
	   } catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   if(ret == 1)
		   return 1;
	   else
		   return 0;
	}
	
	public void run()
	{
		Socket echoSocket = null;
		String token[];
		PrintWriter out = null;
		BufferedReader in = null;
		String content="";
		String requestRFC;
		
		try {
			echoSocket = new Socket(hostname, port);
			out = new PrintWriter(echoSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
			out.println(request);
			requestRFC=rfcno;
			content = in.readLine();
			if(content.contains("404") || content.contains("505")) {
				System.out.println("Error: "+content);
				ret = 1;
				return;
			}
			
			BufferedWriter br = new BufferedWriter(new FileWriter(requestRFC+".txt"));
			while(!in.readLine().equals("Data:"));
			 while(!(content = in.readLine()).equals("END")) {
				 br.append(content);
				 br.newLine();
			 }
			 br.close();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
