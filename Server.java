/*
 * Server 
 */

import java.net.*;
import java.io.*;
import java.util.LinkedList;

class ConcurrentServer extends Thread{
	Socket peerSocket;
	ServerSocket serverSocket;
	static LinkedList<PeerList> peerobj;
	static LinkedList<RFCList> rfcobj;
	static LinkedList<LoginList> loginobj;

	public ConcurrentServer() {
		serverSocket = null;
		peerSocket = null;
		peerobj = new LinkedList<PeerList>();
		rfcobj = new LinkedList<RFCList>();
		loginobj = new LinkedList<LoginList>();
	}

	public ConcurrentServer(Socket peerSoc) {
		peerSocket = peerSoc;
	}

	synchronized static void addToPeerList(PeerList peerdata) {		
		// Check for duplication
		for(PeerList peer : peerobj) {
			if(peer.peername.equals(peerdata.peername) && (peer.portno == peerdata.portno))
				return;
		}
		
		peerobj.addFirst(peerdata);
	}

	synchronized static void addToRFCList(RFCList rfcdata)
	{
		// Check for duplication
		for(RFCList rfc : rfcobj) {
			if(rfc.peername.equals(rfcdata.peername) && rfc.rfctitle.equals(rfcdata.rfctitle) && (rfc.rfcno == rfcdata.rfcno))
				return;
		}
		rfcobj.addFirst(rfcdata);		
	}
	
	synchronized static void addToLoginList(LoginList logindata)
	{
		if(loginobj.indexOf(logindata) == -1)
			loginobj.addFirst(logindata);		
	}
	
	synchronized static int searchLoginList(String hostname)
	{
		for(LoginList host : loginobj) {
			if(host.peername.equals(hostname))
				return 0;
		}		
		return 1;
	}
	
	synchronized static void deleteLists(String hostname) {
		for(PeerList peer : peerobj) {
			if(peer.peername.equals(hostname)) {
				peerobj.remove(peer);
				break;
			}
		}	
		for(RFCList rfc : rfcobj) {
			if(rfc.peername.equals(hostname))
				rfcobj.remove(rfc);
		}
	}

	synchronized static void printList(int listtype) {
		if(listtype == 1) {
			System.out.println("**** Peer List ****");
			for(PeerList peer : peerobj) {
				System.out.println(peer.peername+", "+peer.portno);
			}
		}
		else {
			System.out.println("**** RFC List ****");
			for(RFCList rfc : rfcobj) {
				System.out.println(rfc.peername+", "+rfc.rfctitle+", "+rfc.rfcno);
			}
		}
	}

	synchronized String processLookup(int rfcno) {
		int count = 0;
		String response = "200 OK";
		for(RFCList rfc : rfcobj) {
			if(rfc.rfcno == rfcno) {
				for(PeerList peer : peerobj) {
					if(rfc.peername.equals(peer.peername)) {
						response = response + "\r\n" + rfcno + " " + rfc.rfctitle + " " + rfc.peername + " " + peer.portno;
						count++;
						break;
					}
				}
			}
		}

		if(count == 0)
			response = "404 RFC File not found\r\n";
		System.out.println("Lookup Response:"+response);
		return response;
	}

	/*
	 * This loop assumes there exists only one peer in a host machine
	 */
	synchronized String processList() {
		int count = 0;
		String response = "200 OK";
		for(RFCList rfc : rfcobj) {
			for(PeerList peer : peerobj) {
				if(rfc.peername.equals(peer.peername)) {
					response = response + "\r\n" + rfc.rfcno + " " + rfc.rfctitle + " " + rfc.peername + " " + peer.portno;
					count++;
					//break;
				}
			}
		}

		if(count == 0)
			response = "404 Empty RFC Index list\r\n";
		System.out.println("List Response:"+response);
		return response;
	}

	
	public void Master() {
		try {
			serverSocket = new ServerSocket(7734);
		}
		catch (IOException e)
		{
			System.err.println("Master can't listen on port 7734.");
			System.exit(1);
		}

		System.out.println ("Master listens on port 7734");

		while(true)
		{
			try {
				peerSocket = serverSocket.accept();
				Thread thr = new ConcurrentServer(peerSocket);
				thr.start();
			}
			catch (IOException e)
			{
				System.err.println("Connection failed");
				System.exit(1);
			}
		}
	}

	public void run() {
		System.out.println ("Connected established. Ready to accept input");
		PrintWriter out = null;
		BufferedReader cin = null;
		String hostname = null;
		
		try
		{
			out = new PrintWriter(peerSocket.getOutputStream(),  true);
			cin = new BufferedReader( new InputStreamReader( peerSocket.getInputStream()));
			String inputLine, response, rfctitle, version;
			String []token;
			int portno, rfcno, peerid;
			rfctitle = null;
			portno = 0;		
			hostname = peerSocket.getInetAddress().toString(); 
			if(searchLoginList(hostname) == 0) {
				out.println("LOGIN\r\n");
				inputLine = cin.readLine();
				System.out.println("Client: "+inputLine);
			}	
			else {
				System.out.println("Peer has not already registered");
				out.println("REGISTER\r\n");
				peerid = Integer.parseInt(cin.readLine());
				System.out.println("Client: "+peerid);
				loginobj.addFirst(new LoginList(hostname, peerid));
			}

			// Send an ACK to peer to begin the request
			out.println("LOGIN:ACK\r\n");
			inputLine = cin.readLine();
			System.out.println("Client1: "+inputLine);
			
			do {
			inputLine = cin.readLine();	
			
			if(inputLine.startsWith("ADD")) {
				token = inputLine.split(" ");					
				version = token[3];					
				rfcno = Integer.parseInt(token[2]);
				response = null;

				while((inputLine = cin.readLine()) != null) {
					System.out.println("inputLine:" +inputLine);
					token = inputLine.split(" ");
					if(token[0].equals("Host:")) {
						hostname = token[1];
					}
					else if(token[0].equals("Port:")) {
						portno = Integer.parseInt(token[1]);
					}
					else if(token[0].equals("Title:")) {
						rfctitle = inputLine.split(":")[1].trim();
					}
					else if(inputLine.equals("END")) {
						ConcurrentServer.addToRFCList(new RFCList(hostname, rfcno, rfctitle, version));
						ConcurrentServer.addToPeerList(new PeerList(hostname, portno));
						response = "200 OK\r\n" + rfcno + " " + rfctitle + " " + hostname + " " + portno + "\r\nEND\r\n";
						out.println(response);
						printList(1);
						break;
					}					
				}
				continue;
			}								

			else if (inputLine.contains("LOOKUP")) {
				token = inputLine.split(" ");
				rfcno = Integer.parseInt(token[2]);
				version = token[3];
				while(!(inputLine = cin.readLine()).equals("END"));
				inputLine = cin.readLine();
				response = processLookup(rfcno) + "\r\nEND\r\n";
				out.println(response);
			}
			
			else if (inputLine.contains("LIST ALL")) {
				System.out.println("ListAll:"+inputLine);
				token = inputLine.split(" ");
				version = token[2];
				while(!(inputLine = cin.readLine()).equals("END"));
				inputLine = cin.readLine();
				response = processList() + "\r\nEND\r\n";
				System.out.println("List Response:"+response);
				out.println(response);				
			}

			else if (inputLine.contains("FIN")) {
				System.out.println("Peer wants to close the connection");
				response = "FINACK\r\n";
				out.println(response);
				response = cin.readLine();
				break;
			}
			
			} while(true);		
			
			out.close();
			cin.close();
			peerSocket.close();
			deleteLists(hostname);
		}

		catch(Exception e)
		{
			System.out.print("Exception! "+ e.toString());
			deleteLists(hostname);			
			printList(1);
			printList(2);
			
			try {
				out.close();
				cin.close();
				peerSocket.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}
}	

public class Server {
	public static void main(String[] args) throws IOException
	{
		ConcurrentServer server = new ConcurrentServer();
		server.Master();		
	}

}
