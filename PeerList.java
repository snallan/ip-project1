/*
 * PeerList
 * This class represents a data structure containing Peers's name, listening port 
 */

class PeerList {
	String peername;
	int portno;
	
	public PeerList(String peername, int portno) {
		this.peername = peername;
		this.portno = portno;
	}
}